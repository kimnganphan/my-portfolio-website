import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import "./i18n/i18n";
import { store, persistor } from "./services/store/store";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import 'flag-icon-css/css/flag-icons.min.css'
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
