import "./Project.scss";
import { useTranslation } from "react-i18next";
import { icons } from "../../assets/icons";
import { LanguageTrans } from "../../services/constants/Navbar.constant";
import useStateRedux from "../../services/hooks/use-redux-hook";
import { PROJECT_DATA } from "./ProjectData";

import Card from "../UI/Card/Card";
import Title from "../UI/Title/Title";
import { forwardRef } from "react";

const Project = (props, ref) => {
  const { t } = useTranslation();
  const { theme } = useStateRedux();
  return (
    <div data-theme={theme} id="project" className="project" ref={ref}>
      <div className="project-title">
        <Title color="dark" content={t(LanguageTrans.Navbar_Project)} />
      </div>

      <div className="project-content">
        <div className="project-item">
          <h2>{icons.star}REACT</h2>
          <div className="project-cards">
            {PROJECT_DATA.map((project) => {
              return (
                project.type === "react" && (
                  <Card
                    key={project.id}
                    title={project.projectName}
                    src={project.projectImg}
                    liveLink={project.link}
                    git={project.git}
                    content={project.technology}
                  />
                )
              );
            })}
          </div>
        </div>
        <div className="project-item">
          <h2>{icons.star}LANDING PAGE (HTML - CSS - JAVASCRIPT)</h2>
          <div className="project-cards">
            {PROJECT_DATA.map((project) => {
              return (
                project.type === "mockup" && (
                  <Card
                    key={project.id}
                    title={project.projectName}
                    src={project.projectImg}
                    liveLink={project.link}
                    git={project.git}
                    content={project.technology}
                  />
                )
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default forwardRef(Project);
