import Title from "../UI/Title/Title";
import "./Skill.scss";
import { useTranslation } from "react-i18next";
import { LanguageTrans } from "../../services/constants/Navbar.constant";
import { SKILL_CONTENTS } from "../../services/constants/Skill.constant";
import { images } from "../../assets/images";
import { icons } from "../../assets/icons";
import useStateRedux from "../../services/hooks/use-redux-hook";
import { forwardRef } from "react";
const Skill = (props, ref) => {
  const { t } = useTranslation();
  const { theme } = useStateRedux();
  return (
    <div data-theme={theme} className="skill" id="skill" ref={ref}>
      <div className="skill-title">
        <Title color={"dark"} content={t(LanguageTrans.Navbar_Skill)} />
      </div>

      <div className="skill-content">
        <div className="skill-contentLeft">
          <img src={images.frontEnd} alt="front-end" />
        </div>
        <div className="skill-contentRight">
          <h2>{t(SKILL_CONTENTS.Skill_Intro)}:</h2>

          <div className="skill-contentRight-item">
            <div className="sci-title">
              <h3>01.</h3>
              <h3>/ FRONT-END</h3>
            </div>
            <div className="sci-lists">
              <div className="left">
                <span>{icons.html5} HTML</span>
                <span>{icons.css3} CSS</span>
                <span>{icons.js} JAVASCRIPT</span>
                <span>{icons.jquery} JQUERY</span>
              </div>
              <div className="right">
                <span>{icons.reactjs} REACT</span>
                <span>{icons.bootstrap} BOOTSTRAP</span>
                <span>{icons.sass} SASS</span>
              </div>
            </div>
          </div>

          <div className="skill-contentRight-item">
            <div className="sci-title">
              <h3>02.</h3>
              <h3>/ BACK-END</h3>
            </div>
            <div className="sci-lists">
              <div className="left">
                <span>{icons.nodejs} NODEJS</span>
              </div>
              <div className="right">
                <span>{icons.mongo} MONGODB</span>
              </div>
            </div>
          </div>

          <div className="skill-contentRight-item">
            <div className="sci-title">
              <h3>03.</h3>
              <h3>/ {t(SKILL_CONTENTS.Skill_Other)}</h3>
            </div>
            <div className="sci-lists">
              <div className="left">
                <span>{icons.git} GIT</span>
              </div>
              <div className="right">
                <span>{icons.firebase} FIREBASE</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default forwardRef(Skill);
