import { useTranslation } from "react-i18next";
import useStateRedux from "../../../services/hooks/use-redux-hook";
import "./Card.scss";
import { PROJECT_CONTENTS } from "../../../services/constants/Project.constant";

const Card = ({ src, liveLink, git, content, title }) => {
  const { theme } = useStateRedux();
  const { t } = useTranslation();
  return (
    <div data-theme={theme} className="card">
      <div className="card-top">
        <img src={src} alt="" />
      </div>
      <div className="card-content">
        <h3>{title}</h3>
      </div>
      <div className="card-bottom">
        <a href={liveLink} target="_blank" rel="noopener noreferrer">
          {t(PROJECT_CONTENTS.Project_LivePreview)}
        </a>
        <a href={git} target="_blank" rel="noopener noreferrer">
          Git
        </a>
      </div>
    </div>
  );
};

export default Card;
