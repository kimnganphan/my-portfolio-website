import "./Title.scss";

import useStateRedux from "../../../services/hooks/use-redux-hook";
const Title = ({ content, color }) => {
  const { theme } = useStateRedux();

  return (
    <div data-theme={theme} className={`title ${color}`}>
      {content}
    </div>
  );
};

export default Title;
