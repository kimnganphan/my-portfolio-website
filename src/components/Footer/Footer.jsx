import useStateRedux from '../../services/hooks/use-redux-hook'
import './Footer.scss'

const Footer = () => {
    const {theme} = useStateRedux()
  return (
    <footer data-theme={theme}>Designed By K.Ngan</footer>
  )
}

export default Footer