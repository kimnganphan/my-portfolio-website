import "./Header.scss";
import { images } from "../../assets/images";
import { icons } from "../../assets/icons";
import { LanguageTrans } from "../../services/constants/Header.constant";
import { useTranslation } from "react-i18next";

import useStateRedux from "../../services/hooks/use-redux-hook";
import { forwardRef } from "react";


const Header = (props, ref) => {
  const { t } = useTranslation();

  const { theme } = useStateRedux();

  
  return (
    <header data-theme={theme} id="header" ref={ref}>
      <div className="header-left">
        <img src={images.myAvatar} alt="" />
      </div>
      <div className="header-right">
        <div className="header-title">
          <h1>{t(LanguageTrans.Header_Intro)}</h1>
          <h1>{t(LanguageTrans.Header_Name)}</h1>
        </div>

        <div className="header-job">
          <h3>Front-End Web Developer</h3>
        </div>

        <div className="header-desc">
          <p>{t(LanguageTrans.Header_Desc)} </p>
          <p>{t(LanguageTrans.Header_CVIntro)}</p>
        </div>

        <div className="header-buttons">
          <a href={images.EngCV} download>{t(LanguageTrans.Header_EngCV)}</a>
          <a href={images.VietCV} download>{t(LanguageTrans.Header_VietCV)}</a>
          <a href={images.JapCV} download>{t(LanguageTrans.Header_JPCV)}</a>
        </div>

        <div className="header-seeMore">
          <p>{t(LanguageTrans.Header_SeeMore)}</p>
          <p>{icons.arrDown}</p>
        </div>
      </div>
    </header>
  );
};

export default forwardRef(Header);
