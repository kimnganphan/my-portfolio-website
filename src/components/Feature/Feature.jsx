import Title from "../UI/Title/Title";
import "./Feature.scss";
import { useTranslation } from "react-i18next";
import { LanguageTrans } from "../../services/constants/Navbar.constant";
import { images } from "../../assets/images";
import { icons } from "../../assets/icons";
import useStateRedux from "../../services/hooks/use-redux-hook";
import { FEATURE_CONTENTS } from "../../services/constants/Feature.constant";
import { forwardRef } from "react";
const Feature = (props, ref) => {
  const { t } = useTranslation();
  const { theme } = useStateRedux();
  return (
    <div data-theme={theme} className="feature"　id="feature" ref={ref}>
      <div className="feature-title">
        <Title color={"dark"} content={t(LanguageTrans.Navbar_Feature)} />
      </div>

      <div className="feature-content">
        <img src={images.huongSenClient} alt="feature-project" />

        <div className="feature-description">
          <div className="feaDes-top">
            <h2>{t(FEATURE_CONTENTS.Feature_Title)}</h2>
            <p>{t(FEATURE_CONTENTS.Feature_Intro)}</p>
          </div>
          <div className="feaDes-bottom">
            <a href="https://huongsen.netlify.app/" target="_blank" rel="noopener noreferrer">{t(FEATURE_CONTENTS.Feature_GoToPj)}</a>
            <span>{icons.arrRight}</span>
          </div>
        </div>
      </div>

      <div className="feature-button">
        <a href="#project">{t(FEATURE_CONTENTS.Feature_ViewMorePj)}</a>
      </div>
    </div>
  );
};

export default forwardRef(Feature);
