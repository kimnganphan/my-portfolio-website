import "./Navbar.scss";
import {
  LanguageTrans,
  LANGUAGES_OPTION,
} from "../../services/constants/Navbar.constant";
import { useTranslation } from "react-i18next";
import { icons } from "../../assets/icons";
import { setTheme } from "../../services/store/theme/Theme.slice";
import { useState, useEffect, useRef, useCallback } from "react";
import { setLang } from "../../services/store/language/Language.slice";
import useStateRedux from "../../services/hooks/use-redux-hook";
import useEventListener from "../../services/hooks/use-eventListener-hook";
import useScreenSizeChange from "../../services/hooks/use-screensize-hook";
import i18next from "i18next";

const Navbar = ({...componentRef}) => {
  const navRef = useRef();
  const navLeft = useRef();
  const navLeftWrapper = useRef();
  const navRightRef = useRef();
  const navRightLangRef = useRef();

  const [scrollNav, setScrollNav] = useState("");
  const [activeNavLeft, setActiveNavLeft] = useState("");

  const [navSection, setNavSection] = useState("");
  //DROPDOWN
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [countryName, setCountryName] = useState(LanguageTrans.Navbar_English);

  //TRANSLATION
  const { t } = useTranslation();
  //REDUX
  const { theme, lang, dispatch } = useStateRedux();

  
  //HANDLE NAVBAR SCROLL EVENT
  const onNavScroll = () => {
    if (window.scrollY > 0) {
      setScrollNav("scrollNav");
    } else {
      setScrollNav("");
    }

    if (
      window.scrollY > componentRef.header.current.offsetTop &&
      window.scrollY < componentRef.skill.current.offsetTop
    ) {
      setNavSection("feature");
    } else if (
      window.scrollY > componentRef.feature.current.offsetTop &&
      window.scrollY < componentRef.project.current.offsetTop
    ) {
      setNavSection("skill");
    } else if (
      window.scrollY > componentRef.skill.current.offsetTop &&
      window.scrollY < componentRef.contact.current.offsetTop
    ) {
      setNavSection("project");
    } else if (window.scrollY > componentRef.contact.current.offsetTop) {
      setNavSection("contact");
    } else {
      setNavSection("");
    }
  };
  useEventListener("scroll", onNavScroll);
  //HANDLE CHANGE THEME
  const handleChangeTheme = (type) => {
    if (type === "light") {
      dispatch(setTheme(type));
    } else if (type === "dark") {
      dispatch(setTheme(type));
    }
  };
  //HANDLE ACTIVE NAV MENU
  const handleNavMenu = (section) => {
    setNavSection(section);
  };
  //HANDLE DROP DOWN CHOOSE LANGUAGE
  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = (type) => {
    dispatch(setLang(type.country_code));

    setIsOpen(false);
    if (type.country_code === "gb") {
      i18next.changeLanguage("en");
    } else {
      i18next.changeLanguage(type.country_code);
    }
  };
  useEffect(() => {
    if (lang === "gb") {
      setCountryName(LanguageTrans.Navbar_English);
    } else if (lang === "jp") {
      setCountryName(LanguageTrans.Navbar_Japan);
    } else if (lang === "vn") {
      setCountryName(LanguageTrans.Navbar_Viet);
    }
  }, [lang]);

  useEffect(() => {
    setSelectedOption(
      <div>
        <span className={`flag-icon flag-icon-${lang}`}></span>
        {t(countryName)}
      </div>
    );
  }, [t, lang, countryName]);

  //HANDLE OPEN AND CLOSE MENU WHEN RESPONSIVE
  const handleMenu = (action) => {
    if (action === "open") {
      setActiveNavLeft("activeNav");
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
      setActiveNavLeft("");
    }
  };

  //HANDLE SCREEN SIZE CHANGE

  const handleScreenMatch = useCallback((e) => {
    if (e.matches) {
      navLeftWrapper.current.appendChild(navRightLangRef.current);
    } else {
      navRightRef.current.appendChild(navRightLangRef.current);
    }
  }, []);

  useScreenSizeChange("700", handleScreenMatch);
  return (
    <nav
      ref={navRef}
      data-theme={theme}
      data-language={lang}
      className={scrollNav}
    >
      {/* ---------- NAV MENU ----------*/}

      <span className="nav-menu" onClick={() => handleMenu("open")}>
        {icons.menu}
      </span>
      {/* ---------- END NAV MENU ----------*/}

      {/* ------- NAV LEFT ------- */}
      <div ref={navLeft} className={`nav-left ${activeNavLeft}`}>
        <div ref={navLeftWrapper} className="navLeft-wrapper">
          <span className="nav-close" onClick={() => handleMenu("close")}>
            {icons.close}
          </span>
          <a
            href="#feature"
            onClick={() => handleNavMenu("feature")}
            className={navSection === "feature" ? `activeSec` : ""}
          >
            {t(LanguageTrans.Navbar_Feature)}
          </a>
          <a
            href="#skill"
            onClick={() => handleNavMenu("skill")}
            className={navSection === "skill" ? `activeSec` : ""}
          >
            {t(LanguageTrans.Navbar_Skill)}
          </a>
          <a
            href="#project"
            onClick={() => handleNavMenu("project")}
            className={navSection === "project" ? `activeSec` : ""}
          >
            {t(LanguageTrans.Navbar_Project)}
          </a>
          <a
            href="#contact"
            onClick={() => handleNavMenu("contact")}
            className={navSection === "contact" ? `activeSec` : ""}
          >
            {t(LanguageTrans.Navbar_Contact)}
          </a>
        </div>
      </div>
      {/* ------- END NAV LEFT ------- */}

      {/* ------- NAV LOGO ------- */}
      <div className="nav-logo">K.NGAN</div>
      {/* -------END NAV LOGO ------- */}

      {/* ------- NAV RIGHT ------- */}
      <div ref={navRightRef} className="nav-right">
        <div className="navRight-theme">
          <p>{t(LanguageTrans.Navbar_ThemeTitle)}</p>
          <div className="theme-icon">
            <span
              className={theme === "light" ? `activeTheme` : ``}
              onClick={() => handleChangeTheme("light")}
            >
              {icons.sun}
            </span>
            <span
              className={theme === "dark" ? `activeTheme` : ``}
              onClick={() => handleChangeTheme("dark")}
            >
              {icons.moon}
            </span>
          </div>
        </div>

        <div ref={navRightLangRef} className="navRight-languages">
          <p>{t(LanguageTrans.Navbar_LangTitle)}</p>

          <div className="dropdown-container">
            <div className="dropdown-header" onClick={toggling}>
              {selectedOption || (
                <div>
                  <span className={`flag-icon flag-icon-gb`}></span>
                  {t(countryName)}
                </div>
              )}
            </div>

            {isOpen && (
              <div className="dropdown-listContainer">
                <ul>
                  {LANGUAGES_OPTION.map((option) => {
                    return (
                      <li
                        key={option.id}
                        onClick={() => onOptionClicked(option)}
                      >
                        <span
                          className={`flag-icon flag-icon-${option.country_code}`}
                        ></span>
                        {t(option.country_name)}
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
          </div>
        </div>
      </div>
      {/* ------- END NAV RIGHT ------- */}
    </nav>
  );
};

export default Navbar;
