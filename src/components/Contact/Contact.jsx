import "./Contact.scss";
import Title from "../UI/Title/Title";
import { useTranslation } from "react-i18next";
import { CONTACT_CONTENT } from "../../services/constants/Contact.constant";
import { LanguageTrans } from "../../services/constants/Navbar.constant";
import { icons } from "../../assets/icons";
import useStateRedux from "../../services/hooks/use-redux-hook";
import { forwardRef } from "react";
const Contact = (props, ref) => {
  const { t } = useTranslation();
  const { theme } = useStateRedux();

  return (
    <div data-theme={theme} className="contact" id="contact" ref={ref}>
      <div className="contact-title">
        <Title content={t(LanguageTrans.Navbar_Contact)} />
        <p>{t(CONTACT_CONTENT.Contact_Intro)}</p>
      </div>

      <div className="contact-content">
        <div className="contact-contentLeft">
          <div className="contact-contentLeft-item">
            <h4>
              {icons.phone} {t(CONTACT_CONTENT.Contact_Phone)}
            </h4>
            <p>{t(CONTACT_CONTENT.Contact_PhoneNumber)}</p>
          </div>

          <div className="contact-contentLeft-item">
            <h4>
              {icons.mail} {t(CONTACT_CONTENT.Contact_Email)}
            </h4>
            <p>{t(CONTACT_CONTENT.Contact_EmailAdd)}</p>
          </div>
        </div>
        <div className="contact-contentRight">
          <div className="contact-contentRight-item">
            <input
              type="text"
              placeholder={t(CONTACT_CONTENT.Contact_NamePH)}
            />
          </div>

          <div className="contact-contentRight-item">
            <input
              type="text"
              placeholder={t(CONTACT_CONTENT.Contact_PhonePH)}
            />
          </div>

          <div className="contact-contentRight-item">
            <input
              type="text"
              placeholder={t(CONTACT_CONTENT.Contact_EmailPH)}
            />
          </div>

          <div className="contact-contentRight-item">
            <textarea
              placeholder={t(CONTACT_CONTENT.Contact_MessagePH)}
              id=""
              cols="30"
              rows="10"
            ></textarea>
          </div>

          <div className="contact-contentRight-item">
            <button>{t(CONTACT_CONTENT.Contact_Send)}</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default forwardRef(Contact);
