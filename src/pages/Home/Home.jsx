import { Fragment, useEffect, useRef } from "react";
import "./Home.scss";

import Navbar from "../../components/Navbar/Navbar";
import Header from "../../components/Header/Header";
import Feature from "../../components/Feature/Feature";
import Skill from "../../components/Skill/Skill";
import Project from "../../components/Project/Project";
import Contact from "../../components/Contact/Contact";
import Footer from "../../components/Footer/Footer";

import useStateRedux from "../../services/hooks/use-redux-hook";

const Home = () => {
  const { theme, lang } = useStateRedux();

  const headerRef = useRef();
  const featureRef = useRef();
  const skillRef = useRef();
  const projectRef = useRef();
  const contactRef = useRef();
  
  const componentRef = {
    header: headerRef,
    feature: featureRef,
    skill: skillRef,
    project: projectRef,
    contact: contactRef
  };

  return (
    <Fragment>
      <Navbar {...componentRef} />
      <div data-theme={theme} data-lang={lang} className="home-content">
        <Header ref={headerRef} />
        <Feature ref={featureRef}/>
        <Skill ref={skillRef} />
        <Project ref={projectRef}/>
        <Contact ref={contactRef}/>
        <Footer />
      </div>
    </Fragment>
  );
};

export default Home;
