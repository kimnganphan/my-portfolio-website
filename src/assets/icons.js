import { BsSun, BsMoonStars,BsPhoneFill } from "react-icons/bs";
import { AiOutlineArrowDown, AiFillHtml5, AiFillGitlab, AiOutlineMail, AiOutlineClose, AiOutlineMenu, AiOutlineArrowRight, AiFillStar } from "react-icons/ai";
import { DiCss3, DiJqueryLogo } from "react-icons/di";
import { IoLogoJavascript } from "react-icons/io";
import { FaReact, FaBootstrap, FaSass, FaNodeJs } from "react-icons/fa";
import { SiMongodb,SiFirebase } from "react-icons/si";

export const icons = {
  sun: <BsSun />,
  moon: <BsMoonStars />,
  arrDown: <AiOutlineArrowDown />,
  html5: <AiFillHtml5 />,
  css3: <DiCss3 />,
  js: <IoLogoJavascript />,
  reactjs: <FaReact />,
  jquery: <DiJqueryLogo />,
  bootstrap: <FaBootstrap />,
  sass: <FaSass />,
  nodejs: <FaNodeJs />,
  mongo: <SiMongodb />,
  git: <AiFillGitlab />,
  firebase: <SiFirebase />,
  phone: <BsPhoneFill/>,
  mail: <AiOutlineMail/>,
  close: <AiOutlineClose/>,
  menu :<AiOutlineMenu/>,
  arrRight: <AiOutlineArrowRight/>,
  star : <AiFillStar/>
};
