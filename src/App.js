import { Suspense } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
function App() {
  return (
    <Router>
      <Suspense fallback="loading">
        <Routes>
          <Route path="/" element={<Home />}>
            <Route index element={<Home />} />
            <Route path=":lang" element={<Home />} />
          </Route>
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
