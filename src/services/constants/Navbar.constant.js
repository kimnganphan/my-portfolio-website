export const LanguageTrans = {
  Navbar_Feature: "NAV.Navbar_Feature",
  Navbar_Skill: "NAV.Navbar_Skill",
  Navbar_Project: "NAV.Navbar_Project",
  Navbar_Contact: "NAV.Navbar_Contact",
  Navbar_Light: "NAV.Navbar_Light",
  Navbar_Dark: "NAV.Navbar_Dark",
  Navbar_English: "NAV.Navbar_English",
  Navbar_Viet: "NAV.Navbar_Viet",
  Navbar_Japan: "NAV.Navbar_Japan",
  Navbar_ThemeTitle: "NAV.Navbar_ThemeTitle",
  Navbar_LangTitle: "NAV.Navbar_LangTitle",
};
export const LANGUAGES_OPTION = [
  {
    id: 1,
    country_code: "gb",
    country_name: LanguageTrans.Navbar_English,
  },
  {
    id: 2,
    country_code: "vn",
    country_name: LanguageTrans.Navbar_Viet,
  },
  {
    id: 3,
    country_code: "jp",
    country_name: LanguageTrans.Navbar_Japan,
  },
];
