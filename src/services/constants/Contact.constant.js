export const CONTACT_CONTENT = {
    Contact_Intro: "CONTACT.Contact_Intro",
    Contact_Phone: "CONTACT.Contact_Phone",
    Contact_PhoneNumber: "032-958-7292",
    Contact_Email: "Email",
    Contact_EmailAdd: "kimnganphan24794@gmail.com",
    Contact_NamePH: "CONTACT.Contact_NamePH",
    Contact_PhonePH: "CONTACT.Contact_PhonePH",
    Contact_EmailPH: "CONTACT.Contact_EmailPH",
    Contact_MessagePH: "CONTACT.Contact_MessagePH",
    Contact_Send: "CONTACT.Contact_Send",
}