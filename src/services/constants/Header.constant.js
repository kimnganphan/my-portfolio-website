export const LanguageTrans = {
    Header_Intro: "HEADER.Header_Intro",
    Header_Name: "HEADER.Header_Name",
    Header_Job: "HEADER.Header_Job",
    Header_Desc: "HEADER.Header_Desc",
    Header_CVIntro: "HEADER.Header_CVIntro",
    Header_EngCV: "HEADER.Header_EngCV",
    Header_VietCV: "HEADER.Header_VietCV",
    Header_JPCV: "HEADER.Header_JPCV",
    Header_SeeMore: "HEADER.Header_SeeMore"
}