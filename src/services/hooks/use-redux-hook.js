import { useSelector, useDispatch } from "react-redux";

const useStateRedux = () => {
    const theme = useSelector(state => state.theme.theme)
    const lang = useSelector(state => state.language.language)
    const dispatch = useDispatch()
    
    return {theme, lang, dispatch}
}

export default useStateRedux;